export const initialState = {
  gamePoints: {
    player1: 0,
    player2: 0,
  },
};

export function setScore(playerNumber, previousState) {
  let playerScore = Object.assign({}, previousState.gamePoints, {
    [`player${playerNumber}`]: previousState.gamePoints[`player${playerNumber}`] + 1
  });
  // Current state check if double deuce reset
  if ((playerScore.player1 === 4) && (playerScore.player2 === 4)) {
    playerScore = { player1: 3, player2: 3 };
  }
  else if ((playerScore.player1 > 5) || (playerScore.player2 > 5)) {
    playerScore = { player1: 0, player2: 0 };
  }
  return {
    gamePoints: playerScore
  };
}

let scores = {
  0: () => "love",
  1: () => "15",
  2: () => "30",
  3: () => "40",
  4: () => "AD",
  5: () => " "
}

export function composeScore(gamePoints) {
  //shortcut for points for player1 and player2
  let v1 = gamePoints.player1;
  let v2 = gamePoints.player2;

  let result, winner;

  if ((v1 === v2) && (v1 < 4) && (v2 < 4)) {
    if ((v1 === 3) && (v2 === 3)) {
      result = "Deuce";
    }
    else {
      result = `${scores[v1]()}-all`;
    }
  }
  else if ((v1 === 4) && (v2 < 3)) {
    result = "Game, player1";
     winner = "player1";
  }
  else if ((v2 === 4) && (v1 < 3)) {
    result = "Game, player2";
     winner = "player2";
  }
  else if (v1 > 4) {
    result = "Game, player1";
     winner = "player1";
  }
  else if (v2 > 4) {
    result = "Game, player2";
     winner = "player2";
  }
  else if ((v1 === 4) && (v2 === 4)) {
    result = "Deuce";
  }
  else {
    result = `${scores[v1]()}-${scores[v2]()}`;
  }
  return {
    scoreCall: result, winningPlayer: winner 
  };
}

export function getGameScore(gamePoints) {
  let { scoreCall, winningPlayer } = composeScore(gamePoints);
  return {
    scoreCall,
    winningPlayer,
  };
}
