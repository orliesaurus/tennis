import React, { Component } from 'react';
import { setScore, getGameScore, initialState } from '../scoreboard.js';


class Scoreboard extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;

  }

  makeScore(playerNumber) {
    this.setState(
      (state) => {
        return setScore(playerNumber, state);
      })
    
  }
  
  render() {
    return (
      <div>
        <h1>Tennis Scoreboard</h1>
        {/* This calls the getGameScore function and passes it the current gamePoints and returns the score label */}
        <h2 id="score">Score: {getGameScore(this.state.gamePoints).scoreCall}</h2>
        <button onClick={ () => this.makeScore(1)} className="player1-scores" type="button">
          Player 1 scores
        </button>
        <button onClick={ () => this.makeScore(2)} className="player2-scores" type="button">
          Player 2 scores
        </button>
      </div>
    );
  };
}

export default Scoreboard;
